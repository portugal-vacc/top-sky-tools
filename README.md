# Top Sky Tools

[![Project badge](https://gitlab.com/NetoSimoes/aup_uup-parser/badges/master/pipeline.svg)](https://gitlab.com/NetoSimoes/aup_uup-parser/-/commits/master)

##  [AUP/UUP Parser](https://topskyparser.herokuapp.com/parser)
### Objective
This tool wants to automate the parsing of RSA Areas from Eurocontrol. The output follows the TopSky format.
### Raw Data
Insert with latest AUP/UUP data from [NOP](https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/)
#### Procedure
 1. On Eurocontroll website, open the desired EUUP/EAUP 
 2. Select RSA Allocations 
 3. Ctrl + A  (Select All)
 4. Ctrl + C (Copy to clipboard)
 5. Paste on *Raw Data* text box
### Country ICAO Code
[ICAO Country Codes](https://en.wikipedia.org/wiki/ICAO_airport_code)
### User Text Output
Toggles the output of the *User Text* field
### Safe Level Calculation
- Applies to Portuguese airspace:
  - Fly 1000ft above.
  - Above FL410, flight level should be uneven and vertical separation greater than 2000ft.
### Output
AreaName:SchedStartDate:SchedEndDate:SchedWeekdays:StartTime:EndTime[:Lower:Upper:UserText]
You can find this on the Top Sky *Developer Guide* on section *2.3 TopSkyAreasManualAct.txt*
### Examples
>Valid WEF	29/04/2021 13:30

>Valid TIL	30/04/2021 06:00

#### Calculates Safe Level and Safe Altitude
> Data: LPR42B	 	 	000	015	13:30	21:00	 	LPPC  

> Output: R42B:210429:210429:0:1330:2100:0:1500:SFA2500

> Data: LPR42B	 	 	000	500	13:30	21:00	 	LPPC  	

> Output: R42B:210429:210429:0:1330:2100:0:50000:SFL530

#### Can ommit the UserText section
> Data: LPR42B	 	 	010	070	13:30	16:00	 	LPPC	  

> Output: R42B:210429:210429:0:1330:1600:1000:7000

#### Can ouput simple UserText
> Data: LPR42B	 	 	010	070	13:30	16:00	 	LPPC	  

> Output: R42B:210429:210429:0:1330:1600:1000:7000:10-70

#### Can ouput full RSA Are Name
> Data: LPR42B	 	 	010	070	13:30	16:00	 	LPPC	  

> Output: LPR42B:210429:210429:0:1330:1600:1000:7000

#### Calculates correct End Date based on End Time
> Data: LPR42B	 	 	000	015	13:30	03:00	 	LPPC  

> Output: R42B:210429:210430:0:1330:0300:0:1500:SFA2500
