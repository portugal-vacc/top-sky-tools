from flask import Blueprint, abort

from src.blueprints.ipmametar.ipma import get_metar

bp = Blueprint('ipmametar', __name__, template_folder='templates',
               static_folder='static', url_prefix="/metar")


@bp.route('/<icao>', methods=['GET'])
def metar_icao(icao: str):
    metars = get_metar(icao)
    return metars
