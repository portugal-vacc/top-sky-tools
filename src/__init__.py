import os

from flask import Flask

from src.config import configure_app
from src.routes import configure_routes


def configure_blueprints(app):
    from src.blueprints import aupuupparser_bp
    app.register_blueprint(aupuupparser_bp)

    from src.blueprints import ipmametar_bp
    app.register_blueprint(ipmametar_bp)


def create_app():
    app = Flask('TopSkyTools', template_folder='src/templates', static_folder='src/static')

    configure_app(app, os.environ.get('FLASK_ENV'))
    configure_blueprints(app)
    configure_routes(app)

    return app
