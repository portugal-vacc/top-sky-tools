import datetime
import re
from typing import List, Iterator, Dict

from src.exeptions import NoDataFoundForCode


class RSARawDataParser:
    def __init__(self, raw_data: str):
        self.raw_data = raw_data

    def get_lines_with_code(self, icao_code: str) -> Iterator[str]:
        splitted_data = self.raw_data.upper().split('  ')
        regex_expression = r'\b' + icao_code.upper() + r'\w+'

        if not re.findall(regex_expression, self.raw_data):
            raise NoDataFoundForCode(icao_code)

        for line in splitted_data:
            if re.search(regex_expression, line):
                yield line


class RSANoticeParser:
    def __init__(self, raw_data: str):
        self.raw_data = raw_data

    @property
    def _uupfulldate(self) -> List:
        return re.findall(
            r'\d{2}\/\d{2}\/\d{4}\s\d{2}[:]\d{2}', self.raw_data)

    @property
    def rsa_notice_start_date(self) -> datetime.datetime:
        return datetime.datetime.strptime(self._uupfulldate[0], '%d/%m/%Y %H:%M')

    @property
    def rsa_notice_end_date(self) -> datetime.datetime:
        return datetime.datetime.strptime(self._uupfulldate[1], '%d/%m/%Y %H:%M')

    @property
    def arguments(self) -> Dict:
        return {"start_date": self.rsa_notice_start_date,
                "end_date": self.rsa_notice_end_date}


def get_line_area_arguments(line_data: str) -> Dict:
    """convert string rsa area arguments to dict"""
    data = (item for item in line_data.split('\t'))

    """I'm not proud of this... This exists due to different copy formatting for chrome and firefox
    First item in firefox is empty. If empty we get next item from generator"""
    name = next(data)
    if len(name) <= 3:
        name = next(data)

    return {"name": str(name).strip(),
            "notam": str(next(data)),
            "remark": str(next(data)),
            "min_fl": int(next(data)),
            "max_fl": int(next(data)),
            "start_time": datetime.datetime.strptime((next(data)), '%H:%M'),
            "end_time": datetime.datetime.strptime((next(data)), '%H:%M'),
            "fua": str(next(data)),
            "fir": str(next(data)),
            "uir": str(next(data))}
