from dataclasses import dataclass, field
from datetime import timedelta, datetime
from math import ceil, floor

from src.tools.aupuupparser.rsa_notice import RSANotice


@dataclass
class RSAArea:
    name: str = field(default="LPTEST")
    notam: str = field(default_factory=str)
    remark: str = field(default_factory=str)
    min_fl: int = 0
    max_fl: int = 999
    start_time: datetime = datetime.utcnow()
    end_time: datetime = datetime.utcnow() + timedelta(hours=2)
    fua: str = field(default_factory=str)
    fir: str = field(default_factory=str)
    uir: str = field(default_factory=str)

    def __post_init__(self):
        assert type(self.name) == str
        assert len(self.name) > 2
        assert type(self.min_fl) == int
        assert self.min_fl >= 0
        assert type(self.max_fl) == int
        assert self.max_fl <= 999
        assert self.max_fl > self.min_fl
        assert type(self.start_time) == datetime
        assert type(self.end_time) == datetime
        if not self.end_time > self.start_time:
            self.end_time = self.end_time.replace(day=2)


TOP_CONTROLLED_LEVEL = 660
BOTTOM_CONTROLLED_LEVEL = 5
TRANSITION_ALTITUDE = 4000


class RSAAreaTopSky:
    def __init__(self, rsa_area: RSAArea, rsa_notice: RSANotice):
        self.rsa_area = rsa_area
        self.rsa_notice = rsa_notice

    def name(self, short_name: bool) -> str:
        return self.rsa_area.name[2:] if short_name else self.rsa_area.name

    @property
    def start_date(self) -> str:
        return self.rsa_notice.start_date.strftime("%y%m%d")

    @property
    def end_date(self) -> str:
        if self.rsa_area.end_time.day >= 2:
            return self.rsa_notice.end_date.strftime("%y%m%d")
        return self.rsa_notice.start_date.strftime("%y%m%d")

    @property
    def week_days(self) -> str:
        return "0"

    @property
    def start_time(self) -> str:
        return self.rsa_area.start_time.strftime("%H%M")

    @property
    def end_time(self) -> str:
        return self.rsa_area.end_time.strftime("%H%M")

    @property
    def lower(self) -> str:
        return str(self.rsa_area.min_fl * 100)

    @property
    def upper(self) -> str:
        return str(self.rsa_area.max_fl * 100)

    @staticmethod
    def ceil_flight_level(flight_level: int) -> int:
        return ceil(flight_level / 10) * 10

    @staticmethod
    def floor_flight_level(flight_level: int) -> int:
        return floor(flight_level / 10) * 10

    @property
    def top_required_separation(self):
        return 20 if self.rsa_area.max_fl >= 410 else 10

    @property
    def bottom_required_separation(self):
        return 20 if self.rsa_area.min_fl >= 410 else 10

    @property
    def bottom_area_safe_level(self):
        if self.is_above_transition_altitude(self.rsa_area.min_fl):
            return self.floor_flight_level(self.rsa_area.min_fl) - self.bottom_required_separation
        return self.rsa_area.min_fl - self.bottom_required_separation

    @property
    def top_area_safe_level(self):
        return self.ceil_flight_level(self.rsa_area.max_fl) + self.top_required_separation

    def calculate_max(self):
        if (self.top_area_safe_level > TOP_CONTROLLED_LEVEL) and (
                self.bottom_area_safe_level >= BOTTOM_CONTROLLED_LEVEL):
            return f"{self.formatter(self.bottom_area_safe_level)}MAX"
        return None

    @staticmethod
    def is_above_transition_altitude(level: int) -> bool:
        transition_altitude_two_digit = TRANSITION_ALTITUDE / 100
        if level > transition_altitude_two_digit:
            return True
        return False

    @staticmethod
    def format_for_altitude(altitude: int) -> str:
        altitude_as_string = str(altitude * 100)
        string_len = len(altitude_as_string)
        if string_len < 4:
            for _ in range(4 - string_len):
                altitude_as_string = "0" + altitude_as_string
        return altitude_as_string

    @staticmethod
    def format_for_level(level: int) -> str:
        level_as_string = str(level)
        string_len = len(level_as_string)
        if string_len < 3:
            for _ in range(3 - string_len):
                level_as_string = "0" + level_as_string
        return level_as_string

    def formatter(self, level: int):
        if self.is_above_transition_altitude(level):
            return f"SFL{self.format_for_level(level)}"
        return f"SFA{self.format_for_altitude(level)}"

    @property
    def safe_level_text(self) -> str:
        if self.calculate_max():
            return self.calculate_max()

        if self.top_area_safe_level > TOP_CONTROLLED_LEVEL and self.bottom_area_safe_level < BOTTOM_CONTROLLED_LEVEL:
            return "UNL"

        return self.formatter(self.top_area_safe_level)

    def user_text(self, safe_level: bool) -> str:
        if safe_level:
            return self.safe_level_text
        return f"{self.rsa_area.min_fl} ~ {self.rsa_area.max_fl}"

    def output(self, short_name: bool = True, user_text: bool = True, safe_level: bool = True) -> dict:
        base = {"name": self.name(short_name),
                "start_date": self.start_date,
                "end_date": self.end_date,
                "week_days": self.week_days,
                "start_time": self.start_time,
                "end_time": self.end_time,
                "lower": self.lower,
                "upper": self.upper}
        if user_text:
            base['user_text'] = self.user_text(safe_level)
        return base
