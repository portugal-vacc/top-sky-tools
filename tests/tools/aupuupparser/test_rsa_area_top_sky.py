import pytest

from src.tools.aupuupparser.rsa_area import RSAArea, RSAAreaTopSky
from src.tools.aupuupparser.rsa_notice import RSANotice


def test_area_default():
    area = RSAArea(name="LPONE")
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert output['name'] == 'ONE'
    assert output['user_text']
    for value in output.values():
        assert "None" not in value
        assert " " not in value


def test_area_short_name_false():
    area = RSAArea(name="LPTWO")
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output(short_name=False)
    assert output['name'] == 'LPTWO'


def test_area_user_text_false():
    area = RSAArea()
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output(user_text=False)
    with pytest.raises(KeyError):
        output['user_text']


def test_area_safe_level_false():
    area = RSAArea()
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output(safe_level=False)
    assert "~" in output['user_text']
